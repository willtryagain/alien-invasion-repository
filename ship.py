import pygame

class Ship():
	
	def __init__(self, ai_settings, screen):
		"""Initialize the ship and set its starting position."""
		self.screen = screen
		self.ai_settings = ai_settings
		self.bg_color = (230, 230, 230)
		
		# Load the ship image and get its rect. bmp
		self.image = pygame.image.load('images/ship.bmp')
		self.rect = self.image.get_rect()
		self.screen_rect = screen.get_rect()
		
		# Start each new ship at the bottom center of the screen.
		self.rect.centerx = self.screen_rect.centerx
		self.rect.bottom = self.screen_rect.bottom
		
		# Store a decimal value for the ship's center.
		self.center = float(self.rect.centery)
		
		# Movement flag
		self.moving_right = False
		self.moving_left = False
		
	def update(self):
		"""Update the ship's position based on the movement flags."""
		# Update the ship's center value not the rect.
		if self.moving_right and self.rect.right < self.screen_rect.right: 
			self.center += self.ai_settings.ship_speed_factor
		elif self.moving_left and self.rect.left > 0:
			self.center -= self.ai_settings.ship_speed_factor
		
		# Update rec object from self.center.
		self.rect.centerx = self.center
	def blitme(self):
		"""Draw the ship at its current location."""
		self.screen.blit(self.image, self.rect)
